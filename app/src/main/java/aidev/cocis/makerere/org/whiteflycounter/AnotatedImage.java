package aidev.cocis.makerere.org.whiteflycounter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.hitomi.tilibrary.TransferImage;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static org.opencv.imgproc.Imgproc.putText;

/**
 * Part of this code replicates the code in the main activity
 * A bit of inefficient design due to the fact that the main activity
 * directly calls the camera intent
 */
public class AnotatedImage extends AppCompatActivity {

    public static final String TAG = "wflycount";
    static final int REQUEST_TAKE_IMG = 22222;
    private static final String FILE_LOCATION = Environment.getExternalStorageDirectory() + "/WhiteflyCount/";
    protected TransferImage transferImage;
    Mat src2;
//    String matImage;
    String errorMsg;
    String fieldNum2;
    String imageFileName, field;
    private ImageView wflyImage;
    private Button showResultsButton;
    private Button deleteImageButton;
    private Button startNewFieldButton;
    private Button mEditButton;
    private String imgPath;
    private Uri fileUri;
    private String imagePath;
    private String imagePathNew;
    private int whiteflyCount;
    private TextView wflycountTextView;
    private CascadeClassifier haarCascade;
    private int wflycount; //For consecutive wfly count
    private ArrayList<String> wflyRects;
    private double scale = 1.02;
    private int neighbours = 3;
    private double minimum1 = 10.0;
    private double maximum1 = 25.0;
    private BaseLoaderCallback mOpenCVCallBack = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS: {
                    Log.i(TAG, "OpenCV loaded successfully");
                    try {
                        InputStream is = getResources().openRawResource(R.raw.wf_cascade);
                        File cascadeDir = getDir("cascade", Context.MODE_PRIVATE);
                        File mCascadeFile = new File(cascadeDir, "cascade.xml");
                        FileOutputStream os = new FileOutputStream(mCascadeFile);
                        byte[] buffer = new byte[4096];
                        int bytesRead;

                        Log.i(TAG, "Cas = :" + mCascadeFile.getAbsolutePath());
                        while ((bytesRead = is.read(buffer)) != -1) {
                            os.write(buffer, 0, bytesRead);
                        }
                        is.close();
                        os.close();
                        haarCascade = new CascadeClassifier(mCascadeFile.getAbsolutePath());

                        if (haarCascade.empty())
                            Log.i("Cascade:", "");
                    } catch (Exception e) {
                        Log.i("Cascade Error: ", "Cascade not found");
                    }
                }
                break;
                default:
                    super.onManagerConnected(status);
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anotated_image);
        transferImage = TransferImage.getDefault(this);

        wflyImage = (ImageView) findViewById(R.id.wflyImg);
        showResultsButton = (Button) findViewById(R.id.show_results_button);
        deleteImageButton = (Button) findViewById(R.id.delete_button);
        startNewFieldButton = (Button) findViewById(R.id.new_field_button);
        mEditButton = (Button) findViewById(R.id.edit_button);
        wflycountTextView = (TextView) findViewById(R.id.wflycountnum);

        Bundle fromCameraIntent = getIntent().getExtras();
        imgPath = fromCameraIntent.getString("WFLY_PATH");
        whiteflyCount = fromCameraIntent.getInt("WFLY_COUNT");
        fieldNum2 = fromCameraIntent.getString("FIELD_NUM");
        wflyRects = fromCameraIntent.getStringArrayList("ANNOTATIONS");
//        matImage = fromCameraIntent.getString("MAT_IMAGE");
        wflycountTextView.setText("" + whiteflyCount);

        SharedPreferences sharedpreferences = getSharedPreferences(MainActivity.MyPREFERENCES, Context.MODE_PRIVATE);
        field = sharedpreferences.getString("field", null);


        Glide.with(wflyImage.getContext())
                .load(imgPath)
                .into(wflyImage);

        wflyImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TransferImage.Builder(AnotatedImage.this)
                        .setBackgroundColor(Color.parseColor("#158d3d"))
                        .setImageUrls(wflyImage.toString())
                        .setOriginImages(wflyImage)
                        .setup(transferImage)
                        .show();
            }
        });


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startCameraIntent2();
            }
        });

        showResultsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent showResultsIntent = new Intent(getApplicationContext(), ShowResults.class);
                showResultsIntent.putExtra("WFLY_IMG_PATH", imgPath);
                startActivity(showResultsIntent);
            }


        });

        startNewFieldButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
            }
        });

        deleteImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteImage(imgPath);
            }


        });

        mEditButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent editIntent = new Intent(getApplicationContext(), EditAnnotationsActivity.class);
//                editIntent.putExtra("MAT_IMAGE", matImage);
                editIntent.putExtra("WFLY_IMG_PTH", imgPath);
                editIntent.putExtra("COUNT", wflycount);
                editIntent.putExtra("ANNOTATIONS", wflyRects);
                startActivity(editIntent);
            }
        });

        //Shared preferences
        SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        String svalue = SP.getString("Dvalues", "1.02");
        String neighbors = SP.getString("Mthds", "2");
        String minimum = SP.getString("Min", "10.0");
        String maximum = SP.getString("Max", "25.0");

        if (minimum == null) {

            minimum1 = 10.0;
        } else {

            minimum1 = Double.parseDouble(minimum);
        }

        if (maximum == null) {

            maximum1 = 25.0;
        } else {

            maximum1 = Double.parseDouble(maximum);
        }

        if (svalue == null) {

            scale = 1.02;
        } else {

            scale = Double.parseDouble(svalue);
        }

        if (neighbors == null) {

            neighbours = 2;
        } else {

            neighbours = Integer.parseInt(neighbors);
        }

        if (Double.parseDouble(minimum) > Double.parseDouble(maximum)) {

            UiUtils.showMessage("Minimum cannot be greater than Maximum :)", AnotatedImage.this);
        }
    }

    /*
    * Show a confirmation dialog. On confirmation ("Delete"), the
    * photo is deleted and the activity finishes.
    */
    private void deleteImage(final String imagePath) {
        final File imageURI = new File(imagePath);
        final AlertDialog.Builder alert = new AlertDialog.Builder(
                AnotatedImage.this);
        alert.setTitle(R.string.image_delete_prompt_title);
        alert.setMessage(R.string.image_delete_prompt_message);
        alert.setCancelable(false);
        alert.setPositiveButton(R.string.delete,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(final DialogInterface dialog,
                                        final int which) {
                        imageURI.delete();
                        finish();
                    }
                });
        alert.setNegativeButton(android.R.string.cancel, null);
        alert.show();
    }

    public void startCameraIntent2() {
        Intent takePictureIntent2 = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        File photoFile = null;
        try {
            photoFile = createImageFile();
        } catch (IOException ex) {
            // Error occurred while creating the File
            Toast toast = Toast.makeText(getApplicationContext(), "There was a problem saving the photo...", Toast.LENGTH_SHORT);
            toast.show();
        }
        // Continue only if the File was successfully created
        if (photoFile != null) {
            Uri fileUri = Uri.fromFile(photoFile);
            imagePathNew = fileUri.getPath();
            takePictureIntent2.putExtra(MediaStore.EXTRA_OUTPUT,
                    fileUri);
            startActivityForResult(takePictureIntent2, REQUEST_TAKE_IMG);
        }
    }

    /**
     * The activity returns with the photo.
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_TAKE_IMG && resultCode == Activity.RESULT_OK) {

            Bitmap selectedImage2 = BitmapFactory.decodeFile(imagePathNew);

            src2 = new Mat(selectedImage2.getHeight(), selectedImage2.getWidth(), CvType.CV_8UC4);
            Utils.bitmapToMat(selectedImage2, src2);
            detectWhiteflies2();

//            saveToFile(imagePath, selectedImage);
//
//            Intent showWfly = new Intent(getApplicationContext(), AnotatedImage.class);
//            showWfly.putExtra("WFLY_PATH", imagePath);
//
//            startActivity(showWfly);

        } else {
            Toast.makeText(getApplicationContext(), "Image Capture Failed", Toast.LENGTH_SHORT)
                    .show();
        }
    }

    /**
     * Creates the image file to which the image must be saved.
     *
     * @return
     * @throws IOException
     */
    protected File createImageFile() throws IOException {
        // Create an image file name

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        imageFileName = field + "_" + timeStamp + ".jpg";

        File storageDir = new File(FILE_LOCATION);
        if (!storageDir.exists()) {
            storageDir.mkdir();
        }
        /*File image = File.createTempFile(
                imageFileName,  *//* prefix *//*
                ".jpg",         *//* suffix *//*
                storageDir      *//* directory *//*
        );*/

        File image = new File(storageDir, imageFileName);

        return image;
    }

    public void writeToFile(String data) {

        File file = new File(FILE_LOCATION, "#Counted" + ".txt");

        if (file.exists()) {
            try {
                FileWriter writer = new FileWriter(file, true);
                writer.write(String.format("%20s %12s \r\n", imageFileName, data));
                writer.flush();
                writer.close();

            } catch (IOException e) {
                Log.e("Exception", "File write failed: " + e.toString());
            }
        } else {
            try {
                file.createNewFile();
                FileOutputStream fOut = new FileOutputStream(file);
                OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
                myOutWriter.append(String.format("%20s %20s \r\n", "FIlENAME", "COUNT"));
                myOutWriter.append(String.format("%20s %15s \r\n", imageFileName, data));

                myOutWriter.close();

                fOut.flush();
                fOut.close();
            } catch (IOException e) {
                Log.e("Exception", "File write failed: " + e.toString());
            }
        }
    }

    public void saveToFile(String filename, Bitmap bmp) {

        File countFile = null;

        countFile = new File(new File(FILE_LOCATION), filename + "_count" + ".txt");

        String countPath = null;
        if (countFile != null) {
            Uri fileUri = Uri.fromFile(countFile);
            countPath = fileUri.getPath();
        } else {

        }

        try {
            FileOutputStream out = new FileOutputStream(filename);
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
            FileOutputStream percentageStream = new FileOutputStream(countPath);
            OutputStreamWriter writer = new OutputStreamWriter(percentageStream);
            Writer writer1 = new BufferedWriter(writer);
            writer1.write(wflycount);
            writer1.write(imagePath);
            writer1.close();
            writer.close();
            percentageStream.flush();
            percentageStream.close();
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, percentageStream);
            percentageStream.flush();
            percentageStream.close();
        } catch (Exception e) {
        }
        /**
         * Hack: Save whitefly count and field num in unused Exif properties
         * Use TAG_MODEL to save whitefly count
         * Use TAG_DATETIME to save the date the image was taken
         * Use TAG_MAKE to save the field name/num
         */

        String fieldName = "Field Name/Num: " + fieldNum2;

        String dateTaken = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date());
        ExifInterface exif = null;
        try {
            exif = new ExifInterface(filename);
            exif.setAttribute(ExifInterface.TAG_MODEL, "" + wflycount);
            exif.setAttribute(ExifInterface.TAG_DATETIME, dateTaken);
            exif.setAttribute(ExifInterface.TAG_MAKE, fieldName);
            exif.saveAttributes();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void detectWhiteflies2(){

        new AsyncTask<Void, Void, Bitmap>() {
            ProgressDialog dialog;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                dialog = ProgressDialog.show(AnotatedImage.this, "Detecting white flies", "Please Wait");
            }

            @Override
            protected Bitmap doInBackground(Void... params) {

                String message = "Whiteflies";

                //Detecting face in the frame
                Mat srcGray = new Mat();
                Imgproc.cvtColor(src2, srcGray, Imgproc.COLOR_BGR2GRAY);
                double w = (double) srcGray.width();
                double imgsize_scaler = w / 768.0;
                double low_size = imgsize_scaler * minimum1;
                double upper_size = imgsize_scaler * maximum1;

                MatOfRect wflys = new MatOfRect();
                if(haarCascade != null)
                {
//                    System.out.println("hello Solomon");
                    //haarCascade.detectMultiScale(srcGray, wflys, 1.02, 2, 2, new Size(10,10), new Size(25,25));
                    haarCascade.detectMultiScale(srcGray, wflys, scale, neighbours, 1, new Size(low_size, low_size), new Size(upper_size, upper_size));

                }

                Rect[] wflysArray = wflys.toArray();
                for (int i = 0; i < wflysArray.length; i++)
                    //Core.rectangle(src2, wflysArray[i].tl(), wflysArray[i].br(), new Scalar(100), 3);
                    Imgproc.rectangle(src2, wflysArray[i].tl(), wflysArray[i].br(), new Scalar(100), 3);
                wflycount = wflysArray.length;
                putText(src2, String.valueOf(wflycount) + " " + message, new Point(100, 100), 3, 3, new Scalar(255, 255, 255), 6);
                System.out.println("decount:" + wflycount);

                // Convert resultant CV mat into a bitmap for saving and retrieval
                Bitmap annotatedImage = Bitmap.createBitmap(src2.cols(), src2.rows(), Bitmap.Config.ARGB_8888);

                Utils.matToBitmap(src2, annotatedImage);


                return annotatedImage;
            }

            @Override
            protected void onPostExecute(Bitmap annotatedImage) {
                super.onPostExecute(annotatedImage);
                dialog.dismiss();

                if(annotatedImage!=null) {

                    saveToFile(imagePathNew, annotatedImage);
                    writeToFile(String.valueOf(wflycount));
                    wflycountTextView.setText(""+wflycount);


                    Glide.with(wflyImage.getContext())
                            .load(imagePathNew)
                            .into(wflyImage);

                    /*Intent showWfly = new Intent(getApplicationContext(), AnotatedImage.class);
                    showWfly.putExtra("WFLY_PATH", imagePathNew);
                    showWfly.putExtra("WFLY_COUNT", wflycount);
                    startActivity(showWfly);*/

                } else if (errorMsg != null){
                    Toast.makeText(getApplicationContext(), errorMsg, Toast.LENGTH_SHORT).show();
                }
            }
        }.execute();
    }

    @Override
    protected void onResume() {
        super.onResume();
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this,
                mOpenCVCallBack);
    }


    @Override
    public void onBackPressed() {

        if (transferImage != null && transferImage.isShown()) {
            transferImage.dismiss();
        } else {
            super.onBackPressed();
        }
    }

}