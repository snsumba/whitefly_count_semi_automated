package aidev.cocis.makerere.org.whiteflycounter;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.media.ExifInterface;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import static org.opencv.imgproc.Imgproc.putText;


public class EditAnnotationsActivity extends AppCompatActivity {

    private final String TAG = "EditAnnotationsActivity";
    private String imgPath;
    private int whiteFlyCount;
    private ArrayList<String> wflyRects;
    private Mat leafImage;
    private Bitmap bitmap;

//    private TextView mWhiteFlyCount;
    private String mMode;
    private ImageView mImageView;

    private Button mAddButton;
    private Button mRemoveButtom;
    private Button mSaveButton;
    private CustomImageView mCustomImageView;
    private Button mShowEditedResults;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_annotations);
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);

//        mImageView = (ImageView) findViewById(R.id.leaf_image);
        mCustomImageView = (CustomImageView) findViewById(R.id.leaf_image);
//        mWhiteFlyCount = (TextView) findViewById(R.id.white_fly_count);
        mAddButton = (Button) findViewById(R.id.add_button);
        mRemoveButtom = (Button) findViewById(R.id.remove_button);
        mSaveButton = (Button) findViewById(R.id.save_button);
        mShowEditedResults = (Button) findViewById(R.id.show_edited_results);
//        mMode = (TextView) findViewById(R.id.current_mode);

        Bundle fromAnnotatedImage = getIntent().getExtras();
        imgPath = fromAnnotatedImage.getString("WFLY_IMG_PTH");
//        whiteFlyCount = fromAnnotatedImage.getInt("COUNT");

//        Glide.with(mCustomImageView.getContext())
//                .load(imgPath)
//                .into(mCustomImageView);
//        bitmap = decodeFile(imgPath, mCustomImageView.getWidth(), mCustomImageView.getHeight());
        wflyRects = fromAnnotatedImage.getStringArrayList("ANNOTATIONS");
        drawDetectedWhiteFlies();
        bitmap = Bitmap.createBitmap(leafImage.cols(), leafImage.rows(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(leafImage, bitmap);
        mCustomImageView.setImageBitmap(bitmap);

//        //Set scaling factor so that annotations fit in the editing Image View.
////        Log.d(TAG, "Src height: " + MainActivity.src.height() + "\n" + "Src width: " + MainActivity.src.width());
////        Log.d(TAG, "Image height: " + mCustomImageView.getDrawable().getIntrinsicHeight() + "\n" +
////                "Image width: " + mCustomImageView.getDrawable().getIntrinsicWidth());
//
//        scaleFactorHeight = 400.0f/MainActivity.src.height();
//        scaleFactorWidth = 1.0f*MainActivity.src.width()/mCustomImageView.getDrawable().getIntrinsicWidth();
////        scaleFactorHeight = 1.0f*fromAnnotatedImage.getInt("MAT_HEIGHT")/mCustomImageView.getHeight();
////        scaleFactorWidth = 1.0f*fromAnnotatedImage.getInt("MAT_WIDTH")/mCustomImageView.getWidth();
//
//        //Draw the already detected white-flies.
//        wflyRects = fromAnnotatedImage.getStringArrayList("ANNOTATIONS");
//        mCustomImageView.drawExistingWhiteFlyAnnotations(wflyRects, scaleFactorWidth, scaleFactorHeight);
//        Log.d(TAG, "Scale factor height: " + scaleFactorHeight + "\n" +
//                "Scale factor width: " + scaleFactorWidth+ "\n" + wflyRects.toString());

        whiteFlyCount = wflyRects.size();
//        mWhiteFlyCount.setText(String.format(Integer.toString(whiteFlyCount), Locale.US));

        mCustomImageView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                CustomImageView tryThisOut = (CustomImageView) view;

                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    float xClick = motionEvent.getX();
                    float yClick = motionEvent.getY();

                    //Scale the clicked coordinates to fit the Mat src.
                    float scaleWidth = 1.0f*leafImage.width()/view.getWidth();
                    float scaleHeight = 1.0f*leafImage.height()/view.getHeight();

//                    Log.d(TAG, "Width: " + leafImage.width() + "\n Height: " + leafImage.height());

                    float newX = xClick*scaleWidth;
                    float newY = yClick*scaleHeight;

//                    Log.d(TAG, "Old X: " + xClick + "New X: " + newX + "\n Old Y: " + yClick + "New Y: " + newY);

                    if (mMode.equals("Add")) {
                        addAnnotations(newX, newY);
                        drawDetectedWhiteFlies();
//                        Imgproc.rectangle(leafImage, new Point(newX - 20, newY - 20),
//                                new Point(newX + 20, newY + 20), new Scalar(255), 3);
//                        Log.d(TAG, mCustomImageView.getHeight() + " " + mCustomImageView.getWidth());
                        renderImage();
                    }
//                        tryThisOut.addIndicator(xClick - 5, yClick - 5, 10);
                    else if (mMode.equals("Remove")) {
                        removeAnnotations(newX, newY);
                        drawDetectedWhiteFlies();
                        renderImage();
//                        tryThisOut.removeIndicator(xClick, yClick);
                    }
                }

                tryThisOut.invalidate();
//                mWhiteFlyCount.setText(String.format(Integer.toString(wflyRects.size()), Locale.US));
                return true;
            }
        });

        mAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "Add button clicked.");
                mMode = "Add";
                Toast.makeText(EditAnnotationsActivity.this, R.string.info_add, Toast.LENGTH_LONG).show();
            }
        });

        mRemoveButtom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMode = "Remove";
                Toast.makeText(EditAnnotationsActivity.this, R.string.info_remove, Toast.LENGTH_LONG).show();
            }
        });

        mSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Log.d(TAG, "Save button clicked.");
                //Save the image.
                String[] formerImagePath = imgPath.split("/");
                saveImage(loadBitmapFromView(mCustomImageView), formerImagePath[formerImagePath.length - 1]);
                writeToFile(String.valueOf(wflyRects.size()), formerImagePath[formerImagePath.length - 1]);

                Toast.makeText(EditAnnotationsActivity.this, "Image saved", Toast.LENGTH_SHORT).show();
            }
        });

        mShowEditedResults.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent showEditedResultsIntent = new Intent(getApplicationContext(), ShowEditedResults.class);
                showEditedResultsIntent.putExtra("EDITED_WFLY_IMG_PATH", imgPath);
                startActivity(showEditedResultsIntent);
            }
        });

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap decodeFile(String pathName, int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(pathName);

        // Calculate inSampleSize
        opts.inSampleSize = calculateInSampleSize(opts, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        opts.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(pathName, opts);
    }

    private void drawDetectedWhiteFlies() {
//        leafImage = MainActivity.srcImageToEdit.clone();
        leafImage = new Mat(MainActivity.selectedImage.getHeight(), MainActivity.selectedImage.getWidth(), CvType.CV_8S);
        Utils.bitmapToMat(MainActivity.selectedImage, leafImage);

        for (String wflyRect: wflyRects) {
            String[] rect = wflyRect.split(" ");
            Double x = Double.parseDouble(rect[0]);
            Double y = Double.parseDouble(rect[1]);
            Double width = Double.parseDouble(rect[2]);
            Double height = Double.parseDouble(rect[3]);
            Imgproc.rectangle(leafImage,
                    new Point(x, y),
                    new Point(x + width, y + height),
                    new Scalar(255),
                    3);
        }

        putText(leafImage, String.valueOf(wflyRects.size()) + " " + "Whiteflies", new Point(100, 100), 3, 3, new Scalar(255, 255, 255), 6);
    }

    private void renderImage(){
        Bitmap bitmap = Bitmap.createBitmap(leafImage.cols(), leafImage.rows(), Bitmap.Config.RGB_565);
        Utils.matToBitmap(leafImage, bitmap);
//        Log.d(TAG, leafImage.size().toString());
        if (bitmap.getWidth() > bitmap.getHeight()){
            Matrix matrix = new Matrix();
            matrix.postRotate(90);
            Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth(), bitmap.getHeight(), true);
            Bitmap rotatedBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
            mCustomImageView.setImageBitmap(rotatedBitmap);
        }
        else
            mCustomImageView.setImageBitmap(bitmap);

    }

    private void addAnnotations(float xClick, float yClick){
        String newAnnotation;
        newAnnotation = Float.toString(xClick - 20) + " " +
                        Float.toString(yClick - 20) + " " +
                        Float.toString(40) + " " +
                        Float.toString(40);

        wflyRects.add(newAnnotation);
    }

    private void removeAnnotations(float xClick, float yClick){
        int indexToRemove = getMin(xClick, yClick);
        if (indexToRemove != -1)wflyRects.remove(indexToRemove);
    }

    private double distance(float x1, float y1, float x2, float y2){
        return Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y2 - y1, 2));
    }

    //Get the bounding box that is closest to the point which was clicked.
    private int getMin(float xClicked, float yClicked){
        int minIndex = 0;
        double currMin = Double.MAX_VALUE;
        double dist;

        for (int i = 0; i < wflyRects.size(); i++) {
            String[] annotations = wflyRects.get(i).split(" ");
            float centreX = Float.parseFloat(annotations[0]) + Float.parseFloat(annotations[2])/2;
            float centreY = Float.parseFloat(annotations[1]) + Float.parseFloat(annotations[3])/2;
            dist = distance(xClicked, yClicked, centreX, centreY);

            if (dist < currMin) {
                currMin = dist;
                minIndex = i;
                Log.d(TAG, "Clicked: " + xClicked + ", " + yClicked);
                Log.d(TAG, "Centre: " + centreX + ", " + centreY);
            }
        }

        Log.d(TAG, "Min distance: " + currMin);
        if (currMin > 100) return -1;

        if (wflyRects.size() == 0) return -1;
        return minIndex;
    }


    public String saveImage(Bitmap bitmap, String name){
        String tempFilePath = Environment.getExternalStorageDirectory() + "/WhiteflyCount/EditedPics";

        File directory = new File(tempFilePath);
        if (!directory.exists()){
            if (directory.mkdirs()) Log.d(TAG, "Directory created");
            else Log.e(TAG, "Directory not created");
        }

////        if (!directory.exists()){
////            if (!directory.getParentFile().exists()){
////                Log.d(TAG, "Parent directory " + directory.getParent() + " doesn't exist");
////                directory.getParentFile().mkdirs();
////                Log.d(TAG, "Directory created.");
////            }
////        }
////        Log.d(TAG, "directory "+ tempFilePath + "created");
//
////        directory.delete();
//        File tempFile = new File(tempFilePath);
//        try {
////            directory.createNewFile();
//            tempFile = File.createTempFile(name, "", directory);
//        }
//        catch (IOException e){
//            Log.e(TAG, e.getMessage());
//        }
////        Log.d(TAG, "Directory created");
//        Log.d(TAG, name + " " + tempFile.toString());

        int quality = 100;
        try {
//            FileOutputStream outputStream = new FileOutputStream(tempFile);
            FileOutputStream outputStream = new FileOutputStream(tempFilePath + "/" + name);
            BufferedOutputStream buf = new BufferedOutputStream(outputStream);
            bitmap.compress(Bitmap.CompressFormat.JPEG, quality, buf);

            buf.flush();
            buf.close();
        }
        catch (IOException e){
            Log.e(TAG, e.getMessage());
        }

        String fieldName = "Field Name/Num: " + name.split("_")[0];
        String dateTaken = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date());
        ExifInterface exif = null;
        try {
            exif = new ExifInterface(tempFilePath + "/" + name);
            exif.setAttribute(ExifInterface.TAG_MODEL, "" + wflyRects.size());
            exif.setAttribute(ExifInterface.TAG_DATETIME, dateTaken);
            exif.setAttribute(ExifInterface.TAG_MAKE, fieldName);
            exif.saveAttributes();
        } catch (IOException e) {
            e.printStackTrace();
        }

        bitmap.recycle();
        return tempFilePath;
    }

    public Bitmap loadBitmapFromView(ImageView v){
        BitmapDrawable drawable = (BitmapDrawable) v.getDrawable();
        return drawable.getBitmap();
    }

    public void writeToFile(String data, String imageFileName) {
        String location = Environment.getExternalStorageDirectory() + "/WhiteflyCount/EditedPics";

        File file = new File(location, "#Counted" + ".txt");

        if (file.exists()) {
            try {
                FileWriter writer = new FileWriter(file, true);
                writer.write(String.format("%20s %15s \r\n", imageFileName, data));
                writer.flush();
                writer.close();

            } catch (IOException e) {
                Log.e("Exception", "File write failed: " + e.toString());
            }
        } else {
            try {
                file.createNewFile();
                FileOutputStream fOut = new FileOutputStream(file);
                OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
                myOutWriter.append(String.format("%20s %20s \r\n", "FIlENAME", "WHITEFLY_COUNT"));
                myOutWriter.append(String.format("%20s %15s \r\n", imageFileName, data));
                myOutWriter.close();
                fOut.flush();
                fOut.close();
            } catch (IOException e) {
                Log.e("Exception", "File write failed: " + e.toString());
            }
        }
    }
}
