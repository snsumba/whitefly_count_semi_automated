package aidev.cocis.makerere.org.whiteflycounter;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.JsonReader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;
import org.json.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import static org.opencv.imgproc.Imgproc.putText;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public static final String TAG = "wflycount";
    public static final String MyPREFERENCES = "MyPrefs";
    static final int REQUEST_TAKE_PHOTO = 11111; // Activity result key for camera
    private static final String FILE_LOCATION = Environment.getExternalStorageDirectory() + "/WhiteflyCount/";
    private static final int RESULT_SETTINGS = 1;
    static Mat src;
    private Boolean exit = false;
    static Bitmap selectedImage;
    private ArrayList<String> wflyRects;
    String errorMsg;
    String Mthd;
    String imageFileName;
    SharedPreferences sharedpreferences;
    private EditText fieldNum;
    private Uri fileUri;
    private String imagePath;
    private ImageView gifImage;
    private CascadeClassifier haarCascade;
    private int wflycount;
    private String matToSave;
    private String fieldNumber;
    private String percentage;
    private double scale = 1.02;
    private int neighbours = 3;
    private double minimum1 = 10.0;
    private double maximum1 = 25.0;
    public static final int MY_PERMISSIONS_REQUEST_CAMERA = 1;
    public static final int MY_PERMISSIONS_REQUEST_READ_STORAGE = 2;
    public static final int MY_PERMISSIONS_REQUEST_WRITE_STORAGE = 3;
    private FirebaseAnalytics mFirebaseAnalytics;
    private BaseLoaderCallback mOpenCVCallBack = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS: {
                    Log.i(TAG, "OpenCV loaded successfully");
                    try {
                        InputStream is = getResources().openRawResource(R.raw.wf_cascade);
                        File cascadeDir = getDir("cascade", Context.MODE_PRIVATE);
                        File mCascadeFile = new File(cascadeDir, "cascade.xml");
                        FileOutputStream os = new FileOutputStream(mCascadeFile);
                        byte[] buffer = new byte[4096];
                        int bytesRead;

                        Log.i(TAG, "Cas = :" + mCascadeFile.getAbsolutePath());
                        while ((bytesRead = is.read(buffer)) != -1) {
                            os.write(buffer, 0, bytesRead);
                        }
                        is.close();
                        os.close();
                        haarCascade = new CascadeClassifier(mCascadeFile.getAbsolutePath());

                        if (haarCascade.empty())
                            Log.i("Cascade:", "");
                    } catch (Exception e) {
                        Log.i("Cascade Error: ", "Cascade not found");
                    }
                }
                break;
                default:
                    super.onManagerConnected(status);
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        Window window = this.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        }

        fieldNum = (EditText) findViewById(R.id.fieldNumber);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        gifImage = (ImageView) findViewById(R.id.tutorial_gif);


//        Check that user has input a Field Num or Name. Useful for collating results
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "Text in field is" + fieldNum.getText().toString());
                int fieldNumLength = fieldNum.getText().toString().trim().length();
                if (fieldNumLength == 0) {
                    Snackbar.make(view, "Please specify field Number", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                } else if (fieldNumLength != 0) {
                    startCameraIntent(); // open phone camera event

                }

            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        // Use glide to import an animated gif
        Glide.with(gifImage.getContext())
                .load(R.drawable.wfly_gif1)
                .into(gifImage);
        //Shared preferences
        SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        String svalue = SP.getString("Dvalues", "1.02");
        String neighbors = SP.getString("Mthds", "2");
        String minimum = SP.getString("Min", "10.0");
        String maximum = SP.getString("Max", "25.0");


        if (minimum == null) {

            minimum1 = 10.0;
        } else {

            minimum1 = Double.parseDouble(minimum);
        }

        if (maximum == null) {

            maximum1 = 25.0;
        } else {

            maximum1 = Double.parseDouble(maximum);
        }

        if (svalue == null) {

            scale = 1.02;
        } else {

            scale = Double.parseDouble(svalue);
        }

        if (neighbors == null) {

            neighbours = 2;
        } else {

            neighbours = Integer.parseInt(neighbors);
        }

        if (Double.parseDouble(minimum) > Double.parseDouble(maximum)) {

            UiUtils.showMessage("Minimum cannot be greater than Maximum :)", MainActivity.this);
        }

        //permissions handling
        if (ContextCompat.checkSelfPermission(MainActivity.this,
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                    Manifest.permission.CAMERA)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.CAMERA},
                        MY_PERMISSIONS_REQUEST_CAMERA);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);


    }

    @Override
    protected void onResume() {
        super.onResume();
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this,
                mOpenCVCallBack);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
        else if (exit){
            finish();
        }
        else {
            Toast.makeText(this, "Press back again to exit.", Toast.LENGTH_SHORT).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 3*1000);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent i = new Intent(this, UserSettings.class);
            startActivityForResult(i, RESULT_SETTINGS);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
            startCameraIntent();
        } else if (id == R.id.nav_gallery) {

            // Get image at position 1 in the folder with images
            String resultImagePath = "";
            File storageDir = new File(FILE_LOCATION);
            if (storageDir.exists()) {
                File[] listFile = storageDir.listFiles();
                resultImagePath = listFile[1].toString();
            }
            Intent showResultsIntent = new Intent(getApplicationContext(), ShowResults.class);
            showResultsIntent.putExtra("WFLY_IMG_PATH", resultImagePath);
            startActivity(showResultsIntent);

        } else if (id == R.id.nav_manage) {
            Intent i = new Intent(this, UserSettings.class);
            startActivityForResult(i, RESULT_SETTINGS);

        } else if (id == R.id.nav_share) {
            shareTextUrl();

        } else if (id == R.id.nav_send) {
            sendFeedback();
        } else if (id == R.id.nav_about) {

            showAboutDialog();

        } else if (id == R.id.nav_privacy_policy) {

            String url = "http://www.mcrops.org/privacypolicy.html";
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);
        } else if (id == R.id.nav_help) {

            String url = "http://www.mcrops.org/whiteflyhelp.html";
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);
        }



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void startCameraIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        // Create the File where the photo should go.
        // If you don't do this, you may get a crash in some devices.
        File photoFile = null;
        try {
            photoFile = createImageFile(fieldNum.getText().toString());
        } catch (IOException ex) {
            // Error occurred while creating the File
            Toast toast = Toast.makeText(getApplicationContext(), "There was a problem saving the photo...", Toast.LENGTH_SHORT);
            toast.show();
        }
        // Continue only if the File was successfully created
        if (photoFile != null) {
            Uri fileUri = Uri.fromFile(photoFile);
            imagePath = fileUri.getPath();
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                    fileUri);
            startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
        }
    }


    /**
     * The activity returns with the photo.
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == Activity.RESULT_OK) {

            selectedImage = BitmapFactory.decodeFile(imagePath);

            // Whitefly detect code goes here
            //================================
            Log.d(TAG, "Height: " + selectedImage.getHeight() + "\nWidth: " + selectedImage.getWidth());
            if (selectedImage.getWidth() > selectedImage.getHeight()){
                //This rotates the image so that the height is greater than the width.
                //This necessary for the annotating to work properly.
                Matrix matrix = new Matrix();
                matrix.postRotate(90);
                Bitmap scaledBitmap = Bitmap.createScaledBitmap(selectedImage, selectedImage.getWidth(), selectedImage.getHeight(), true);
                selectedImage = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
//                src = new Mat(selectedImage.getHeight(), selectedImage.getWidth(), CvType.CV_8UC4);
//                Utils.bitmapToMat(selectedImage, src);
            }
//            else {
//                src = new Mat(selectedImage.getHeight(), selectedImage.getWidth(), CvType.CV_8UC4);
//                Utils.bitmapToMat(selectedImage, src);
//            }

            src = new Mat(selectedImage.getHeight(), selectedImage.getWidth(), CvType.CV_8UC4);
            Utils.bitmapToMat(selectedImage, src);

            Log.d(TAG, "Mat Height: " + src.height() + "\nMat Width: " + src.width());
//            src = new Mat(selectedImage.getHeight(), selectedImage.getWidth(), CvType.CV_8UC4);
//            Utils.bitmapToMat(selectedImage, src);
            detectWhiteflies();
            //================================

//            saveToFile(imagePath, selectedImage);

//            Intent showWfly = new Intent(getApplicationContext(), AnotatedImage.class);
//            showWfly.putExtra("WFLY_PATH", imagePath);
//
//            startActivity(showWfly);

        } else {
            Toast.makeText(getApplicationContext(), "Image Capture Failed", Toast.LENGTH_SHORT)
                    .show();
        }
    }

    /**
     * Creates the image file to which the image must be saved.
     *
     * @return
     * @throws IOException
     */
    protected File createImageFile(String field) throws IOException {
        // Create an image file name
        //permissions handling
        if (ContextCompat.checkSelfPermission(MainActivity.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_WRITE_STORAGE);
            }
        }

//        System.out.println("field" + field);
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        imageFileName = fieldNum.getText().toString() + "_" + timeStamp + ".jpg";

        File storageDir = new File(FILE_LOCATION);
        if (!storageDir.exists()) {
            storageDir.mkdir();
        }
        /*File image = File.createTempFile(
                imageFileName,  *//* prefix *//*
                ".jpg",         *//* suffix *//*
                storageDir      *//* directory *//*
        );*/

        File image = new File(storageDir, imageFileName);

        return image;
    }


    public void saveToFile(String filename, Bitmap bmp) {
        try {
//            BitmapFactory.Options bounds = new BitmapFactory.Options();
//            bounds.inJustDecodeBounds = true;
//            BitmapFactory.decodeFile(filename, bounds);
//
//            ExifInterface exif = new ExifInterface(filename);
//            String orientString = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
//            int orientation = orientString != null ? Integer.parseInt(orientString) : ExifInterface.ORIENTATION_NORMAL;
//
//            int rotationAngle = 0;
//            if (orientation == ExifInterface.ORIENTATION_ROTATE_90) rotationAngle = 90;
//            if (orientation == ExifInterface.ORIENTATION_ROTATE_180) rotationAngle = 180;
//            if (orientation == ExifInterface.ORIENTATION_ROTATE_270) rotationAngle = 270;
//
//            Matrix matrix = new Matrix();
//            matrix.setRotate(rotationAngle, (float) bmp.getWidth()/2, (float) bmp.getHeight()/2);
//            Bitmap rotatedBitmap = Bitmap.createBitmap(bmp, 0, 0, bounds.outWidth, bounds.outHeight, matrix, true);
            FileOutputStream out = new FileOutputStream(filename);
//            rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        /*
         * Hack: Save whitefly count and field num in unused Exif properties
         * Use TAG_MODEL to save whitefly count
         * Use TAG_DATETIME to save the date the image was taken
         * Use TAG_MAKE to save the field name/num
         */

        String fieldName = "Field Name/Num: " + fieldNum.getText().toString();
        String dateTaken = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date());
        ExifInterface exif = null;
        try {
            exif = new ExifInterface(filename);
            exif.setAttribute(ExifInterface.TAG_MODEL, "" + wflycount);
            exif.setAttribute(ExifInterface.TAG_DATETIME, dateTaken);
            exif.setAttribute(ExifInterface.TAG_MAKE, fieldName);
            exif.saveAttributes();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void detectWhiteflies() {

        AsyncTask<Void, Void, Bitmap> execute = new AsyncTask<Void, Void, Bitmap>() {
            ProgressDialog dialog;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                dialog = ProgressDialog.show(MainActivity.this, "Detecting white flies", "Please Wait");
            }

            @Override
            protected Bitmap doInBackground(Void... params) {
                String message = "Whiteflies";


                //Detecting face in the frame
                Mat srcGray = new Mat();
                Imgproc.cvtColor(src, srcGray, Imgproc.COLOR_BGR2GRAY);

                MatOfRect wflys = new MatOfRect();
                double w = (double) srcGray.width();
                double h = (double) srcGray.height();
                double imgsize_scaler = w / 768.0;
                double low_size = imgsize_scaler * minimum1;
                double upper_size = imgsize_scaler * maximum1;

                if (haarCascade != null) {
                    haarCascade.detectMultiScale(srcGray, wflys, scale, neighbours, 1, new Size(low_size, low_size), new Size(upper_size, upper_size));
                }

                Rect[] wflysArray = wflys.toArray();
                wflyRects = new ArrayList<String>();
//                Log.d(TAG, Arrays.toString(wflysArray));
                for (int i = 0; i < wflysArray.length; i++){
                    //Core.rectangle(src, wflysArray[i].tl(), wflysArray[i].br(), new Scalar(100), 3);
                    Imgproc.rectangle(src, wflysArray[i].tl(), wflysArray[i].br(), new Scalar(100), 3);
//                    wflyRects.add(wflysArray[i].tl() + " " + wflysArray[i].br());
                    wflyRects.add(wflysArray[i].x + " " + wflysArray[i].y + " " + wflysArray[i].width + " " + wflysArray[i].height);
                }

                wflycount = wflysArray.length;
                putText(src, String.valueOf(wflycount) + " " + message, new Point(100, 100), 3, 3, new Scalar(255, 255, 255), 6);

                // Release of matrix objects
                srcGray.release();
                wflys.release();

                // Convert resultant CV mat into a bitmap for saving and retrieval
                Bitmap annotatedImage = Bitmap.createBitmap(src.cols(), src.rows(), Bitmap.Config.ARGB_8888);
                Utils.matToBitmap(src, annotatedImage);

                Bundle bundle = new Bundle();
                bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "whiteflies:"+wflycount);
                bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "main_activity");
                bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "main");
                mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
                //Sets whether analytics collection is enabled for this app on this device.
                mFirebaseAnalytics.setAnalyticsCollectionEnabled(true);

                //Sets the minimum engagement time required before starting a session. The default value is 10000 (10 seconds). Let's make it 20 seconds just for the fun
                mFirebaseAnalytics.setMinimumSessionDuration(20000);

                //Sets the duration of inactivity that terminates the current session. The default value is 1800000 (30 minutes).
                mFirebaseAnalytics.setSessionTimeoutDuration(500);


                mFirebaseAnalytics.setUserId("farmer");

                //Sets a user property to a given value.
                mFirebaseAnalytics.setUserProperty("FARMER","farmer");



                return annotatedImage;
            }

            @Override
            protected void onPostExecute(Bitmap annotatedImage) {
                super.onPostExecute(annotatedImage);
                dialog.dismiss();

                if (annotatedImage != null) {

                    saveToFile(imagePath, annotatedImage);
                    writeToFile(String.valueOf(wflycount));
                    Intent showWfly = new Intent(getApplicationContext(), AnotatedImage.class);
                    showWfly.putExtra("WFLY_PATH", imagePath);
                    showWfly.putExtra("WFLY_COUNT", wflycount);
                    showWfly.putExtra("FIELD_NUM", fieldNum.getText().toString());
                    showWfly.putExtra("ANNOTATIONS", wflyRects);
//                    showWfly.putExtra("MAT_HEIGHT", src.height());
//                    showWfly.putExtra("MAT_WIDTH", src.width());
//                    showWfly.putExtra("MAT_IMAGE", matToSave);

                    startActivity(showWfly);

                } else if (errorMsg != null) {
                    Toast.makeText(getApplicationContext(), errorMsg, Toast.LENGTH_SHORT).show();
                }
            }
        }.execute();
    }

//    //Convert an open CV Mat object to a json object.
//    public static String matToJson(Mat mat) throws JSONException {
//        JSONObject obj = new JSONObject();
//
//        if (mat.isContinuous()){
//            int cols = mat.cols();
//            int rows = mat.rows();
//            int elemSize = (int) mat.elemSize();
//
//            byte[] data = new byte[cols * rows * elemSize];
//
//            mat.get(0, 0, data);
//            obj.put("rows", mat.rows());
//            obj.put("cols", mat.cols());
//            obj.put("type", mat.type());
//
//            // We cannot set binary data to a json object, so:
//            // Encoding data byte array to Base64.
//            String dataString = new String(Base64.encode(data, Base64.DEFAULT));
//            Log.d(TAG, ""+dataString.length());
//            obj.put("data", dataString);
//
////            Gson gson = new Gson();
////            String json = gson.toJson(obj);
//
//            return obj.toString();
//        }
//        else {
//            Log.e(TAG, "Mat not continuous");
//        }
//
//        return "{}";
//    }

//    public static Mat matFromJson(String json) throws JSONException {
//        JSONObject parser = new JSONObject(json);
//
//        int rows = parser.getInt("rows");
//        int cols = parser.getInt("cols");
//        int type = parser.getInt("type");
//
//        String dataString = parser.getString("data");
//        byte[] data = Base64.decode(dataString.getBytes(), Base64.DEFAULT);
//
//        Mat mat = new Mat(rows, cols, type);
//        mat.put(0, 0, data);
//
//        return mat;
//    }

    public void writeToFile(String data) {

        File file = new File(FILE_LOCATION, "#Counted" + ".txt");

        if (file.exists()) {
            try {
                FileWriter writer = new FileWriter(file, true);
                writer.write(String.format("%20s %15s \r\n", imageFileName, data));
                writer.flush();
                writer.close();

            } catch (IOException e) {
                Log.e("Exception", "File write failed: " + e.toString());
            }
        } else {
            try {
                file.createNewFile();
                FileOutputStream fOut = new FileOutputStream(file);
                OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
                myOutWriter.append(String.format("%20s %20s \r\n", "FIlENAME", "WHITEFLY_COUNT"));
                myOutWriter.append(String.format("%20s %15s \r\n", imageFileName, data));
                myOutWriter.close();
                fOut.flush();
                fOut.close();
            } catch (IOException e) {
                Log.e("Exception", "File write failed: " + e.toString());
            }
        }
    }

    private void sendFeedback() {
        final Intent _Intent = new Intent(android.content.Intent.ACTION_SEND);
        _Intent.setType("text/html");
        _Intent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{getString(R.string.mail_feedback_email)});
        _Intent.putExtra(android.content.Intent.EXTRA_SUBJECT, getString(R.string.mail_feedback_subject));
        _Intent.putExtra(android.content.Intent.EXTRA_TEXT, getString(R.string.mail_feedback_message));
        startActivity(Intent.createChooser(_Intent, getString(R.string.title_send_feedback)));
    }

    private void shareTextUrl() {
        Intent share = new Intent(android.content.Intent.ACTION_SEND);
        share.setType("text/plain");
        share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);

        // Add data to the intent, the receiving app will decide
        // what to do with it.
        share.putExtra(Intent.EXTRA_SUBJECT, "Title Of The Post");
        share.putExtra(Intent.EXTRA_TEXT, "air.ug/mcrops");

        startActivity(Intent.createChooser(share, "Share link!"));
    }

    private void showAboutDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppAlertDialog);

        LayoutInflater inflater = this.getLayoutInflater();
        View layout = inflater.inflate(R.layout.bout, null);

        builder.setCancelable(true)
                .setView(layout);

        final AlertDialog aboutDialog = builder.create();

        android.view.WindowManager.LayoutParams langParams = aboutDialog.getWindow().getAttributes();
        langParams.width = GridLayout.LayoutParams.MATCH_PARENT;
        aboutDialog.getWindow().setAttributes(langParams);

        final TextView view = (TextView) layout.findViewById(R.id.text1);
        view.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://mcrops.org"));
                startActivity(browserIntent);
            }

        });


        Button closeBtn = (Button) layout.findViewById(R.id.closeBtn);

        closeBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                aboutDialog.dismiss();
            }
        });

        aboutDialog.show();
    }

    private void showPolicyDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppAlertDialog);

        LayoutInflater inflater = this.getLayoutInflater();
        View layout = inflater.inflate(R.layout.policy, null);

        builder.setCancelable(true)
                .setView(layout);

        final AlertDialog aboutDialog = builder.create();

        android.view.WindowManager.LayoutParams langParams = aboutDialog.getWindow().getAttributes();
        langParams.width = GridLayout.LayoutParams.MATCH_PARENT;
        aboutDialog.getWindow().setAttributes(langParams);

        final TextView view = (TextView) layout.findViewById(R.id.text1);
        view.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://mcrops.org"));
                startActivity(browserIntent);
            }

        });


        Button closeBtn = (Button) layout.findViewById(R.id.closeBtn);

        closeBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                aboutDialog.dismiss();
            }
        });

        aboutDialog.show();
    }

}
