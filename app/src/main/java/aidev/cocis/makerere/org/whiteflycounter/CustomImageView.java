package aidev.cocis.makerere.org.whiteflycounter;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;

//import java.util.ArrayList;
//import java.util.List;

/**
 * It's probably now useless to use this custom Image view since I'm no longer using the canvas API.
 * */

public class CustomImageView extends android.support.v7.widget.AppCompatImageView {

    //    public boolean drawRect = false;
//    private List<WhiteflyIndicator> mWhiteflyIndicators;
//    public float top;
//    public float left;
//    public float bottom;
//    public float right;

    public CustomImageView(Context context, AttributeSet attrs){
        super(context, attrs);

//        mWhiteflyIndicators = new ArrayList<>();
        Paint currentPaint = new Paint();
        currentPaint.setDither(true);
//        currentPaint.setColor(0xFFCC);
        currentPaint.setStyle(Paint.Style.STROKE);
        currentPaint.setStrokeJoin(Paint.Join.ROUND);
        currentPaint.setStrokeWidth(2);
    }

//    public void drawExistingWhiteFlyAnnotations(ArrayList<String> wflys, float scaleFactorWidth, float scaleFactorHeight){
//        for (String wfly: wflys){
//            String[] location = wfly.split(" ");
//            mWhiteflyIndicators.add(new WhiteflyIndicator(
//                    Float.parseFloat(location[0])*scaleFactorHeight,
//                    Float.parseFloat(location[1])*scaleFactorWidth,
//                    Float.parseFloat(location[2])*scaleFactorHeight,
//                    Float.parseFloat(location[3])*scaleFactorWidth));
//        }
//    }

    @Override
    protected void onDraw(Canvas canvas) {

//        canvas.drawColor(Color.TRANSPARENT);
        super.onDraw(canvas);
//        for (WhiteflyIndicator indicator: mWhiteflyIndicators) {
//            float left = indicator.getLeft();
//            float top = indicator.getTop();
//            float right = left + indicator.getWidth();
//            float bottom = top + indicator.getHeight();
//            canvas.drawRect(left, top, right, bottom, currentPaint);
//        }
    }

//    public void addIndicator(float left, float top, float width){
//        WhiteflyIndicator indicator = new WhiteflyIndicator(left, top, width);
//        mWhiteflyIndicators.add(indicator);
//    }
//
//    public void removeIndicator(float xClicked, float yClicked){
//        //Find indicator nearest to where was clicked and remove it.
//        if (!mWhiteflyIndicators.isEmpty()){
//            mWhiteflyIndicators.remove(getMin(xClicked, yClicked));
//        }
//    }
//
//    private double distance(float x1, float y1, float x2, float y2){
//        return Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y2 - y1, 2));
//    }
//
//    //Get the indicator closest to where was clicked.
//    private int getMin(float xClicked, float yClicked){
//        int minIndex = 0;
//        double currMin = Double.MAX_VALUE;
//        double dist;
//
//        for (int i = 0; i < mWhiteflyIndicators.size(); i++) {
//            WhiteflyIndicator indicator = mWhiteflyIndicators.get(i);
//            dist = distance(xClicked, yClicked, indicator.getLeft(), indicator.getTop());
//            if (dist < currMin) {
//                currMin = dist;
//                minIndex = i;
//            }
//        }
//
//        return minIndex;
//    }

//    public int getWhiteflyCount(){
//        return mWhiteflyIndicators.size();
//    }

//    @Override
//    public String toString() {
//        String rep = "";
//        rep += "White-fly count: " + Integer.toString(getWhiteflyCount()) + "\n";
//        rep += "White-fly indicators: " + "\n";
//        for (WhiteflyIndicator indicator: mWhiteflyIndicators) {
//            rep += indicator.toString() + "\n";
//        }
//
//        return rep;
//    }
}
