package aidev.cocis.makerere.org.whiteflycounter;

/**
 * Created by emwebaze on 3/26/16.
 */
public class WflyImage {

    private String name;
    private String image;
    private String dateWfly;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDateWfly() {
        return dateWfly;
    }

    public void setDateWfly(String dateWfly) {
        this.dateWfly = dateWfly;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}