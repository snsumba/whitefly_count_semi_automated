package aidev.cocis.makerere.org.whiteflycounter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

/**
 * Created by emwebaze on 3/26/16.
 */
public class WflyRecyclerView extends RecyclerView.Adapter<WflyRecyclerView.ViewHolder> {

    private List<WflyImage> wflyImageList;
    private ImageView imageToShow;

    public WflyRecyclerView(List<WflyImage> wflyImageList, ImageView showingImage) {
        this.wflyImageList = wflyImageList;
        this.imageToShow = showingImage;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        final WflyImage wflyImage = wflyImageList.get(position);
        holder.mTextView.setText(wflyImage.getName());
        holder.mTextViewWfly.setText(wflyImage.getDateWfly());

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Glide.with(imageToShow.getContext())
                        .load(wflyImage.getImage())
                        .into(imageToShow);

            }
        });

        Glide.with(holder.mImageView.getContext())
                .load(wflyImage.getImage())
                .fitCenter()
                .into(holder.mImageView);
    }

    @Override
    public int getItemCount() {
        return wflyImageList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {


        public final View mView;
        public final ImageView mImageView;
        public final TextView mTextView;
        public final TextView mTextViewWfly;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mImageView = (ImageView) view.findViewById(R.id.img_thumbnail);
            mTextView = (TextView) view.findViewById(R.id.field_num);
            mTextViewWfly = (TextView) view.findViewById(R.id.date_wfly);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mTextView.getText();
        }
    }
}