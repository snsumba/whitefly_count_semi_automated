package aidev.cocis.makerere.org.whiteflycounter;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.media.ExifInterface;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.hitomi.tilibrary.TransferImage;

import aidev.cocis.makerere.org.whiteflycounter.R;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ShowResults extends AppCompatActivity {

    private static final String TAG = "Imgsha";
    public ImageView showingImage;
    protected TransferImage transferImage;
    private String imgPath;
    private ArrayList<String> f = new ArrayList<String>();   // list of available files in  path
    private File[] listFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_results);
        transferImage = TransferImage.getDefault(this);

        showingImage = (ImageView) findViewById(R.id.wflyImgResult);

        //Get image path from previous intent and display it as first result
        Bundle fromAnnotatedIntent = getIntent().getExtras();
        imgPath = fromAnnotatedIntent.getString("WFLY_IMG_PATH");
        if (!imgPath.isEmpty()) {
            displayImage(showingImage, imgPath);
        }

        // Initialize recyclervier
        RecyclerView rv = (RecyclerView) findViewById(R.id.recyclerview);
        setupRecyclerView(rv);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
//                startActivity(new Intent(getApplicationContext(), MainActivity.class));
            }
        });

        showingImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TransferImage.Builder(ShowResults.this)
                        .setBackgroundColor(Color.parseColor("#158d3d"))
                        .setImageUrls(showingImage.toString())
                        .setOriginImages(showingImage)
                        .setup(transferImage)
                        .show();
            }
        });
    }

       /* showingImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("powel");


                loadPhoto(showingImage,0,0);

                *//*final Dialog nagDialog = new Dialog(ShowResults.this,android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
                nagDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                nagDialog.setCancelable(false);
                nagDialog.setContentView(R.layout.preview_image);
                Button btnClose = (Button)nagDialog.findViewById(R.id.btnIvClose);
                showingImage = (ImageView)nagDialog.findViewById(R.id.iv_preview_image);

                btnClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View arg0) {

                        nagDialog.dismiss();
                    }
                });
                nagDialog.show();*//*



            }
        });*/




    /*private void loadPhoto(ImageView imageView, int width, int height) {

        ImageView tempImageView = imageView;


        AlertDialog.Builder imageDialog = new AlertDialog.Builder(this);
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);

        View layout = inflater.inflate(R.layout.custom_fullimage_dialog,
                (ViewGroup) findViewById(R.id.layout_root));
        ImageView image = (ImageView) layout.findViewById(R.id.fullimage);
        image.setImageDrawable(tempImageView.getDrawable());
        imageDialog.setView(layout);
        imageDialog.setPositiveButton(getString(R.string.ok_button), new DialogInterface.OnClickListener(){

            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }

        });


        imageDialog.create();
        imageDialog.show();
    }
*/



    private void setupRecyclerView(RecyclerView recyclerView) {
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        recyclerView.setAdapter(new WflyRecyclerView(wflyImgs(), showingImage));
    }

    public void getSdcardImages() {
        File file = new File(Environment.getExternalStorageDirectory().toString() + "/WhiteflyCount");

        Log.i(TAG, "File path" + file);
        if (file.isDirectory()) {
            listFile = file.listFiles();

            for (File aListFile : listFile) {
                f.add(aListFile.getAbsolutePath());
            }
            Collections.reverse(f);
        }
    }

    private List<WflyImage> wflyImgs() {

        getSdcardImages();

        ExifInterface retExif;
        String fieldName = "Field Num: Unknown";
        String dateNumWflies = "2016-July-28 | ";
        String wflies = "0";
        List<WflyImage> wflyImages = new ArrayList<>();


        for (int i = 0; i < f.size(); i++) {
            String fout = f.get(i);
            if (i != (f.size() - 2)) {
                try {
                    retExif = new ExifInterface(fout);
                    wflies = retExif.getAttribute(ExifInterface.TAG_MODEL);
                    fieldName = retExif.getAttribute(ExifInterface.TAG_MAKE);
                    dateNumWflies = retExif.getAttribute(ExifInterface.TAG_DATETIME) + " | ";
                } catch (IOException e) {

                }
                WflyImage wflyImage = new WflyImage();
                wflyImage.setImage(f.get(i));
                wflyImage.setName(fieldName);
                wflyImage.setDateWfly(dateNumWflies + wflies + " Whiteflies");
                wflyImages.add(wflyImage);
            } else {
            }

        }


        return wflyImages;
    }

    private void displayImage(ImageView imgView, String img_path) {

        System.out.println("tweheyo");

        Glide.with(imgView.getContext())
                .load(img_path)
                .into(imgView);
    }



    /*
 * Show a confirmation dialog. On confirmation ("Delete"), the
 * photo is deleted and the activity finishes.
 */
    private void deleteImage(final String imagePath) {
        final File imageURI = new File(imagePath);
        final AlertDialog.Builder alert = new AlertDialog.Builder(
                ShowResults.this);
        alert.setTitle(R.string.image_delete_prompt_title);
        alert.setMessage(R.string.image_delete_prompt_message);
        alert.setCancelable(false);
        alert.setPositiveButton(R.string.delete,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(final DialogInterface dialog,
                                        final int which) {
                        imageURI.delete();
                        finish();
                    }
                });
        alert.setNegativeButton(android.R.string.cancel, null);
        alert.show();
    }

    @Override
    public void onBackPressed() {

        if (transferImage != null && transferImage.isShown()) {
            transferImage.dismiss();
        } else {
            super.onBackPressed();
        }
    }
}


