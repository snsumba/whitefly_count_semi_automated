package aidev.cocis.makerere.org.whiteflycounter;

import android.content.Intent;
import android.graphics.Color;
import android.media.ExifInterface;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.hitomi.tilibrary.TransferImage;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ShowEditedResults extends AppCompatActivity {

    private static final String TAG = "EditAnnotationsActivity";
    public ImageView showingImage;
    protected TransferImage transferImage;
    private String imgPath;
    private ArrayList<String> filesInPath = new ArrayList<>();
    private File[] listFile;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_results);

        transferImage = TransferImage.getDefault(this);
        showingImage = (ImageView) findViewById(R.id.wflyImgResult);

        //Get image path from previous intent and display it as first result
        Bundle fromEditedIntent = getIntent().getExtras();
        imgPath = fromEditedIntent.getString("EDITED_WFLY_IMG_PATH");

        if (!(imgPath == null))displayImage(showingImage, imgPath);

        // Initialize recyclervier
        RecyclerView rv = (RecyclerView) findViewById(R.id.recyclerview);
        setupRecyclerView(rv);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

        showingImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TransferImage.Builder(ShowEditedResults.this)
                        .setBackgroundColor(Color.parseColor("#158d3d"))
                        .setImageUrls(showingImage.toString())
                        .setOriginImages(showingImage)
                        .setup(transferImage)
                        .show();
            }
        });

    }

    @Override
    public void onBackPressed() {
        Intent main = new Intent(getApplicationContext(), MainActivity.class);
        main.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        main.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        main.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(main);

    }

    private void setupRecyclerView(RecyclerView recyclerView) {
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        recyclerView.setAdapter(new WflyRecyclerView(wflyImgs(), showingImage));
    }

    private void displayImage(ImageView imgView, String img_path) {

        Glide.with(imgView.getContext())
                .load(img_path)
                .into(imgView);
    }

    public void getSdcardImages() {
        File file = new File(Environment.getExternalStorageDirectory().toString() + "/WhiteflyCount/" + "EditedPics");

        Log.i(TAG, "File path" + file);
        if (file.isDirectory()) {
            listFile = file.listFiles();

            for (File aListFile : listFile) {
                filesInPath.add(aListFile.getAbsolutePath());
            }
            Collections.reverse(filesInPath);
        }
    }

    private List<WflyImage> wflyImgs() {

        getSdcardImages();

        ExifInterface retExif;
        String fieldName = "Field Num: Unknown";
        String dateNumWflies = "2017-July-31 | ";
        String wflies = "0";
        List<WflyImage> wflyImages = new ArrayList<>();

        for (int i = 0; i < filesInPath.size(); i++) {
            String fout = filesInPath.get(i);
            Log.d(TAG, fout);
            if (i != (filesInPath.size() - 2)) {
                try {
                    retExif = new ExifInterface(fout);
                    wflies = retExif.getAttribute(ExifInterface.TAG_MODEL);
                    fieldName = retExif.getAttribute(ExifInterface.TAG_MAKE);
                    dateNumWflies = retExif.getAttribute(ExifInterface.TAG_DATETIME) + " | ";
                } catch (IOException e) {
                    Log.e(TAG, "Error in wflyImgs method.");
                }
                WflyImage wflyImage = new WflyImage();
                wflyImage.setImage(filesInPath.get(i));
                wflyImage.setName(fieldName);
                wflyImage.setDateWfly(dateNumWflies + wflies + " Whiteflies");
                wflyImages.add(wflyImage);
            } else {
            }

        }

        return wflyImages;
    }
}
